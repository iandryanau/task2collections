import java.util.HashMap;
import java.util.Map;

public class LRUCache<K, V> {

    private final int capacity;

    private Map<K, Node<K, V>> cacheElements = new HashMap<>();
    private Node<K, V> head;
    private Node<K, V> tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
    }

    private void removeTail() {
        cacheElements.remove(tail.key);

        tail = tail.previous;
        tail.next = null;
    }

    private void moveToHead(Node<K, V> node) {
        if (node == head) {
            return;
        }
        node.previous.next = node.next;
        if (node.next != null) {
            node.next.previous = node.previous;
        } else {
            tail = node.previous;
        }
        putAsHead(node);
    }

    private void putAsHead(Node<K, V> node) {
        node.next = head;
        node.previous = null;

        if (head != null) {
            head.previous = node;
        }

        head = node;

        if (tail == null) {
            tail = head;
        }
    }

    public V get(K key) {

        if (cacheElements.containsKey(key)) {
            final Node<K, V> n = cacheElements.get(key);

            moveToHead(n);

            return n.value;
        }

        return null;
    }

    public void put(K key, V value) {
        if (cacheElements.containsKey(key)) {
            final Node<K, V> old = cacheElements.get(key);
            old.value = value;

            moveToHead(old);
        } else {
            final Node<K, V> created = new Node<>(key, value);

            if (cacheElements.size() >= capacity) {
                removeTail();
                putAsHead(created);
            } else {
                putAsHead(created);
            }

            cacheElements.put(key, created);
        }
    }

    private static class Node<K, V> {
        K key;
        V value;
        Node<K, V> next;
        Node<K, V> previous;

        private Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return "Node [key=" + key + ", value=" + value + "]";
        }

    }

    @Override
    public String toString() {
        return cacheElements.toString();
    }

    public static void main(String[] args) {
        LRUCache<Integer, Integer> cache = new LRUCache<>(2);
        cache.put(3, 2);
        cache.put(4, 3);
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        cache.put(5, 4);
        System.out.println(cache.get(4));
    }
}